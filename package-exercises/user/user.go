package user

import (
	"fmt"
	"strconv"
)

type Person struct {
	Name string
	Age  int
	Sex  string
}

func (object *Person) IsAdult(age int) {
	object.Age = age
	if object.Age >= 18 {
		fmt.Println("Hey, you're a adult! " + strconv.Itoa(age))
	} else if object.Age < 18 {
		fmt.Println("Hey, you're not a adult! " + strconv.Itoa(age))
	}
}
func (object *Person) CheckSex(sex string) {
	object.Sex = sex
	if object.Sex == "F" {
		fmt.Println("You are a Female: " + sex)
	} else {
		fmt.Println("You are a Male: " + sex)
	}
}
func (object *Person) ToString(name string) {
	object.Name = name
	fmt.Println("your name is: " + object.Name)
}
