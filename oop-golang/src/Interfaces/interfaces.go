package main

import (
	"fmt"
	"strconv"
)

var count = 0

type Bank interface {
	deposit(quantity int) string
	withDraw(quantity int) string
}
type Account struct{}

func (b Account) deposit(quantity int) string {
	if quantity > 0 {
		count += quantity
		return " This is the Quantity that you deposit in your account bank: " + strconv.Itoa(count)
	} else {
		return "Please enter a valid quantity"
	}
}
func (b Account) withDraw(quantity int) string {
	if quantity >= 0 {
		count -= quantity
		return "This is the Quantity that your withdraw in your account bank: " + strconv.Itoa(count)
	} else {
		return "Please enter a valid quantity"
	}
}
func main() {
	var bank Bank
	bank = new(Account)
	fmt.Println(bank.deposit(100))
	bank.withDraw(10)
	bank.withDraw(10)
	fmt.Println(bank.withDraw(10))
}
