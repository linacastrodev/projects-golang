package main
import 
(
	"fmt"
	"strconv"

)
// this a scope variable
var counter = 0
//this a struct (is any same to the OOP "class", but this have other funcionality's)
type Account struct{
	titular string
	cantidad int
}
// this methods is any same how the methods in OOP (call Setter's and Getter's)
func (this *Account) set_titular(titular string){
	this.titular = titular
}
// this methods is any same how the methods in OOP (call Setter's and Getter's)
func (this *Account) set_cantidad(cantidad int){
	this.cantidad = cantidad
}
// this methods is any same how the methods in OOP (call Setter's and Getter's)
func (this *Account) get_cantidad() int {
	return this.cantidad
}
// this methods is any same how the methods in OOP (call Setter's and Getter's)
func (this *Account) get_titular() string {
	return this.titular
}
func (this *Account) deposit(cantidad int) string {
	if cantidad > 0 {
		counter += cantidad
		return "This is the Quantity actual in your account \n" + strconv.Itoa(counter)		
	}else{
		return "Please fill with a valid number :D"
	}
} 
func (this *Account) withDraw(cantidad int) string{
	if cantidad > 0 {
		counter -= cantidad
		return " This is the Quantity actual in your account \n" + strconv.Itoa(counter)
	}else{
		return "Please fill with a valid number :D"
	}
}
func main(){
	//we instance the structure Account
	instance := new(Account)
	instance.titular = "Lina"
	//In account you have:
	instance.deposit(30)
	instance.deposit(40)
	// Deposit
	fmt.Println(instance.deposit(30))
	// Withdraw
	fmt.Println(instance.withDraw(10))
}