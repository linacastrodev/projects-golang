package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type comment struct {
	Comment string
	Name    string
}
type comments []comment

func main() {
	fmt.Println("")
	http.HandleFunc("/json", func(writer http.ResponseWriter, response *http.Request) {
		jsonComment := comments{
			comment{"COMENTARIO DE PRUEBA", "LINA"},
			comment{"COMENTARIO DE PRUEBA", "VENUS"},
			comment{"COMENTARIO DE PRUEBA", "LINA"},
			comment{"COMENTARIO DE PRUEBA", "VENUS"},
		}
		json.NewEncoder(writer).Encode(jsonComment)
	})

	http.ListenAndServe(":8000", nil)

}
