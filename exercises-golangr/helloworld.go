package main

import (
	"fmt"
)

type world interface {
	exerciseOne() string
}
type person struct{}

func (user *person) exerciseOne() string {
	var name string
	var lastname string
	fmt.Println("Please Enter your name: ")
	fmt.Scanf("%s\n", &name)
	fmt.Println("Please Enter yor lastname: ")
	fmt.Scanf("%s\n", &lastname)
	return "Welcome, your name is: " + name + " and " + " your lastname is: " + lastname
}

func main() {
	var implements world
	implements = new(person)
	fmt.Println(implements.exerciseOne())
}
