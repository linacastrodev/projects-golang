package main

import (
	"fmt"
)

type comments interface {
	addComments()
}

type user struct {
}

func (user *user) addComments() {
	array := map[string]string{
		"Esto es un comentario1 ":  "Lina",
		"Esto es un comentario2 ":  "Lina",
		"Esto es un comentario3 ":  "Sofia",
		"Esto es un comentario4 ":  "Maria",
		"Esto es un comentario5 ":  "Lina",
		"Esto es un comentario6 ":  "Lina",
		"Esto es un comentario7 ":  "Maria",
		"Esto es un comentario8 ":  "Sofia",
		"Esto es un comentario9 ":  "Manuel",
		"Esto es un comentario10 ": "Lina",
	}
	for i, value := range array {
		if value == "Lina" && value == "Maria" {
			fmt.Println(i, value)
		}
	}
}

func main() {
	var com comments
	com = new(user)
	com.addComments()
}
