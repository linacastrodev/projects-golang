package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	// ioutil -> returns two response : response and err -> the err is basically when the file is not found
	// and the first response if this is found the file (and the file have a response with type in bytes, for this reason, is important change of byte to strings)
	response, err := ioutil.ReadFile("./helloworld.txt")
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(string(response))
	}

}
