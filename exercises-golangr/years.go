package main

import (
	"fmt"
	"strconv"
)

type year interface {
	datos()
	calculate(date int) string
}
type useryear struct{}

func (user *useryear) datos() {
	var date int
	fmt.Println("Please enter your date of your born: ")
	fmt.Scanf("%d\n", &date)
	fmt.Println(user.calculate(date))
}
func (user *useryear) calculate(date int) string {
	var calculate = 2020 - date
	return "Your age actually is: " + strconv.Itoa(calculate)
}
func main() {
	var anio year
	anio = new(useryear)
	anio.datos()
}
